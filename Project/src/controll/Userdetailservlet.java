package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Userdetailservlet
 */
@WebServlet("/Userdetailservlet")
public class Userdetailservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userdetailservlet() {
        super();
        // TODO Auto-generated constructor stub

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");

		// TODO Auto-generated method stub
		//userlist画面の詳細ボタンが押されたらこのdogetメソッドで処理をする
		String id=request.getParameter("id");

		//findall()で検索し入れ込んであるデータをこのサーブレットのセッションにセット
		UserDao userDao = new UserDao();
		User user = userDao.findrow(id);

		// リクエストスコープにユーザ一覧情報をセットここでまとまったインスタンスuserをセットしておけば、JSPファイルでuser.loginIdやuser.nameなどで書けるようになるサーブレットファイルで一々変数を用意しなくて済む。
		request.setAttribute("user", user);

		//userdetailを表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		doGet(request, response);
	}

}
