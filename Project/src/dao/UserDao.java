package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ここのクラスは、サーブレットクラスでここのクラス型のインスタンスを用意し、「○○.このクラスで書いたメソッド名」で、簡単に呼び出されるsql文を扱ったメソッド置き場である。サーブレット、JSPクラスでメソッドは書かない。
 */
@WebServlet("/UserDao")
public class UserDao {

	//ログイン画面セッションのデータ扱い
	public User loginsessions(String loginId, String password) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備（ここではログイン画面で入力した文字列がmodelフォルダのUserクラス（インスタンス）に保存
			 * されているかを探すsql文なのでselect文で且つFROMの対象がuser　テーブルの*(←そのテーブルの全て)と
			 * なっている。でWHEREの条件で、login_id=?とあるが、それはデータベース内に設定されたカラム名login_id
			 * にログイン画面で入力された文字列のデータが存在するかどうかを設定するものである。「＝？」で？に入力された文字列が入る。
			 * ？への入力値の設定は、↓↓のsetStringで設定する。
			*/
			String sql = "SELECT * FROM user WHERE login_id= ? and password=?";

			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, md5(password));
			ResultSet rs = stmt.executeQuery();

			// (loginservlet等では→)ログイン失敗時の処理rsの中にloginIdとpasswordが入ってなかったらnullを返す。
			if (!rs.next()) {
				return null;
			}

			// (loginservlet等では→)ログイン成功時の処理	（↑のifで引っかかってなかったらこの処理に続くつまりlogin_idとname）
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//loginsession 引数1つのメソッドど作成
	public String searchId(String loginId) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備（ここではログイン画面で入力した文字列がmodelフォルダのUserクラス（インスタンス）に保存
			 * されているかを探すsql文なのでselect文で且つFROMの対象がuser　テーブルの*(←そのテーブルの全て)と
			 * なっている。でWHEREの条件で、login_id=?とあるが、それはデータベース内に設定されたカラム名login_id
			 * にログイン画面で入力された文字列のデータが存在するかどうかを設定するものである。「＝？」で？に入力された文字列が入る。
			 * ？への入力値の設定は、↓↓のsetStringで設定する。
			*/
			String sql = "SELECT * FROM user WHERE login_id= ?";

			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			// (loginservlet等では→)ログイン失敗時の処理rsの中にloginIdが入ってなかったらnullを返す。
			if (!rs.next()) {
				return null;
			}

			// (loginservlet等では→)ログイン成功時の処理	（↑のifで引っかかってなかったらこの処理に続くつまりsearchIdメソッドが実行されたらsql実行結果が入ったデータ入れrsからgetStringでlogin_idが入ったloginIdのデータを最終的に返すことになるreturnで）
			loginId = rs.getString("login_id");

			return loginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//loginidをidから検索するメソッド
	public String searchlogId(String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備（ここではログイン画面で入力した文字列がmodelフォルダのUserクラス（インスタンス）に保存
			 * されているかを探すsql文なのでselect文で且つFROMの対象がuser　テーブルの*(←そのテーブルの全て)と
			 * なっている。でWHEREの条件で、login_id=?とあるが、それはデータベース内に設定されたカラム名login_id
			 * にログイン画面で入力された文字列のデータが存在するかどうかを設定するものである。「＝？」で？に入力された文字列が入る。
			 * ？への入力値の設定は、↓↓のsetStringで設定する。
			*/
			String sql = "SELECT login_id FROM user WHERE id= ?";

			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			// (loginservlet等では→)ログイン失敗時の処理rsの中にloginIdが入ってなかったらnullを返す。
			if (!rs.next()) {
				return null;
			}

			// (loginservlet等では→)ログイン成功時の処理	（↑のifで引っかかってなかったらこの処理に続くつまりsearchIdメソッドが実行されたらsql実行結果が入ったデータ入れrsからgetStringでlogin_idが入ったloginIdのデータを最終的に返すことになるreturnで）
			String loginId = rs.getString("login_id");

			return loginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//テーブル内全てのカラムのデータを取得、getstringで取得したいカラム名を設定、そのカラムのデータをUserクラスで作成したインスタンスに値をwhileで上からすべてUser<list>型で用意したuserListに入れ込むメソッド。
	public List<User> findAll() {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			con = DBM.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id  NOT IN('admin')";
			//↑ここ NOT IN('admin')で、カラム名login_idの中でadmin以外のデータを検索という命令文にできる。
			//このように書くことで、検索されたデータが下にあるadd　userlistでlogin＿idがadminを含む行以外の　行のデータが格納される。
			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//データベースに入力した値をINSERTで入れるメソッド。必ず引数を元のデータベースのカラム名の順番と一緒にすること。setString()のカラム順番もこのメソッド内に書くsql文もカラム順番、引数とそろえる事。
	public void useradd(String loginId, String name, String birthDate, String password) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();

			// INSERT文を準備。create_dateとupdate_dateはnow()で登録時、更新時の日付が記録されるので左から何番目のカラムなのかを把握して、↓の様にカラム値に合わせてvalueに記述すること。カラムはA5:SQLで作成したテーブルの内容を見て把握できる。
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) value(?,?,?,?,NOW(),NOW())";

			// INSERTを実行し、入力された値をいれる。inseert文のときは、statementではなくPreparedStatementで且つそこの()内にsql文を入れ、exequteupdateで（）内は必ず空で、sqlを実行する
			stmt = con.prepareStatement(sql);
			// Userインスタンスに設定し、データベースに追加。カラムを必ず元のデータベースのカラム順番にそろえる事
			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, md5(password));

			int rs = stmt.executeUpdate();//↑の様にPreparedStatementでsqlを実行するときは、このexequteupdate()の中は必ず空にすることと決まり切っている。ここにまた入れるとsqlsyntaxエラー(sql文法に関するエラー)が出てものすごく原因が特定しづらくなる。

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

	//自動採番される変数idを引数にし、idでsql　SELECT文で検索。条件はidに入る文字を含んだ行のlogin_id,name,birth_date,create_date,update_dateのデータを取得するという内容で、
	public User findrow(String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備

			*/
			String sql = "SELECT login_id,name,birth_date,create_date,update_date FROM user WHERE id=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			/* sqlのSELECT文は、検索処理をするとき、一番初めに見るところは一番初めに作成したデータよりも前の何もないところを見る。
				だから
				sql文を使う際には(SELECT)必ず始めのデータがなければfalseが帰ってきてあればtuleを返して次に行く
				性質を持つnext()を書くこと。
				!rs.next()でfalseがtrueになり次のデータを参照する。
				書かなければエラーが出る。
			*/
			if (!rs.next()) {
				return null;
			}
			//
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(loginId, name, birthDate, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー情報削除機能
	public void userdelete(String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM user WHERE id= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			int rs = stmt.executeUpdate();

			String ms = "削除しました";

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//ユーザー情報編集(update) passwordを含む更新
	public void updatep(String password, String name, String birthdate, String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備
				UPDATE文idを基にユーザー情報検索し、更新。UPDATEでは、カラム名の指定だけでなく、条件付けのところでも？のバインドキーが設定される必要がある。
				条件を付けないとすべてのカラムで更新されてしまうからである。だから、
				条件を付けてそれに引っかかったカラムがある行のSETで書いたカラム名を指定すれば、指定した行の指定したカラム文更新することができる。
				また、？はSET　WHEREとまたがってついているが、SQL文の終わりである「;」まではまたがってカラム名をカウントするので
				；までの文の初めの左側から1、2、3、とsetstringでカラム値を指定するときに設定すればいいのでWHEREまでまたがってても臆せず、一番左から
				数えてカラム値を設定すること。
			*/
			String sql = "UPDATE user SET name=?,birth_date=?,password=? WHERE id=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, birthdate);
			stmt.setString(3, md5(password));
			stmt.setString(4, id); //WHEREのとこのバインド ;で終わってないので;までは左からカウントしてカラム値を設定すること

			int rs = stmt.executeUpdate();
			//SELECT文ではないし、executeQueryでもないので.next()はいらない。

			//ここのupdateメソッドはvoid型は型を返さないのでreturnもいらない。
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//上記とほぼ同じ(こちらはpasswordを含まない更新)
	public void update(String name, String birthdate, String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {
			//tryの中にdb接続→PrepareStatementでsql文書き込み→stmt.setstirng（）で()内にカラム.入れ込む予定の値を扱うためのkey名を設定→ResultSet方のrsにexecuteQueryで実行した内容の結果を格納といった一連の処理を書く。

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備
				UPDATE文idを基にユーザー情報検索し、更新。UPDATEでは、カラム名の指定だけでなく、条件付けのところでも？のバインドキーが設定される必要がある。
				条件を付けないとすべてのカラムで更新されてしまうからである。だから、
				条件を付けてそれに引っかかったカラムがある行のSETで書いたカラム名を指定すれば、指定した行の指定したカラム文更新することができる。
				また、？はSET　WHEREとまたがってついているが、SQL文の終わりである「;」まではまたがってカラム名をカウントするので
				；までの文の初めの左側から1、2、3、とsetstringでカラム値を指定するときに設定すればいいのでWHEREまでまたがってても臆せず、一番左から
				数えてカラム値を設定すること。
			*/
			String sql = "UPDATE user SET name=?,birth_date=? WHERE id=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, birthdate);
			stmt.setString(3, id); //WHEREのとこのバインド ;で終わってないので;までは左からカウントしてカラム値を設定すること

			int rs = stmt.executeUpdate();
			//SELECT文ではないし、executeQueryでもないので.next()はいらない。

			//ここのupdateメソッドはvoid型は型を返さないのでreturnもいらない。
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//引数passwordを暗号化させるメソッド。sql文の？にメソッド名と引数で入れることができる。
	public String md5(String password) {

		//ハッシュを生成したい元の文字列 String 型のsourceを用意。そこに引数であるパスワードで初期化。このsourceにpasswordの文字列が入ることになる。
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset　文字コードを固定する。
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム　暗号化する仕組みを用意。仕組みはMD5を指定
		String algorithm = "MD5";
		//ハッシュ生成処理、byte型の配列bytesを用意と同時にnullと初期化しないといけないから同時に初期化
		byte[] bytes = null;
		try { //上で用意した配列bytesをmd5で暗号化する仕組みが入ったMessageDigest.getInstance(algorithm)を実行、「.digest(source.getBytes(fharset))」と続いているのでpasswordの文字列が入ったsourceがutf-8の文字コードでBytes型で変換されるという意味。で、変換され、暗号化された
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) { //messagedigestの文では必ずtrycatch文で囲むこと。
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} //↓のこのメソッドで上でいろいろ設定されたbytesを暗号化し、した結果をresultに入れる。
		String result = DatatypeConverter.printHexBinary(bytes);
		//resultを戻り値として返す。このメソッド名とそこに引数を入れれば　sqlの？に暗号化後のデータとして扱える。
		return result;
	}

	//UserList画面の検索に使われる文。この分で、ログインID 　ユーザー名、生年月日のいずれかを入力すれば、そのいずれかの条件一つで検索するためのメソッド。
	public List<User> namesearch(String searchloginId, String searchname, String searchBirthDate, String searchBirthDate2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();


		try {
			// データベースへ接続
			con = DBM.getConnection();
			Statement stmt =con.createStatement();

			/*UserList画面の検索に使われる文。この分で、ログインID 　ユーザー名、生年月日のいずれかを入力すれば、そのいずれかの条件一つで↓の文で検索できる。
			*仕組みとしては、下で用意したlogin＿idがadmin以外の全カラムを検索対象としたsql文を用意。し、if文でuserList画面の入力欄で入力した項目すべてをサーブレット
			*でgetparameterで取得して変数に入れ、その変数をここdaoクラスに書いたこのnamesearchメソッドの引数にして利用する。
			*で、userlist画面で入力したものがgetparameterで値を取得し、変数に入れ、このメソッドの引数にするのだから
			*その変数が空文字だったり、文字列が入っていたりするわけである。
			*それを利用し、下のif文で空文字でなかったら、つまり値が入っていたら、という条件にして、その条件に引っかかったら、
			*
			*用意したsql文が入った変数sqlを呼び出し、+=で文字結合という形で続きのsql文を引っ付けて
			*入力された項目の値を部分一致に利用し、検索させるsql文に完成させるのである。
			*もし、空文字だったらif文に引っかからずに、そのままスルーされるので余分なデータ、文が引っ付かない。
			*で、+=で結合され、完成したsql文が「Statement」形式でexecuteQueryの（）に入り、Resurltset型の変数rsに
			*実行結果が入る。注意すべきは、preparedstatementと違い、？にsetstringで文字列を入れるのではなく、
			*statement形式で、？を使わず、引数をそのまま使って部分一致の為％で挟んでいるのに加えて「’’」←シングルクォーテーションで
			*囲んでいる。statementで検索条件を変数を使って作るときは、必ずこの点に注意すること。
			*後は、whileでgetstringでsqlで検索され、条件にあたはまったデータが取得され、用意した変数に入れられる。
			*/
			String sql = "SELECT * FROM user WHERE login_id  NOT IN('admin')";
			//↑ここ NOT IN('admin')で、カラム名login_idの中でadmin以外のデータを検索という命令文にできる。
			//このように書くことで、検索されたデータが下にあるadd　userlistでlogin＿idがadminを含む行以外の　行のデータが格納される。


			/*このif文で、それぞれの項目で入力されたデータがサーブレットのgetparameterで取得、変数に入り、このメソッドの引数として扱われ、
			*空文字か否かで↑で用意したsql文に追加される文が↓のif文の条件次第で対応した入力項目での部分一致で条件検索させるための文が追加され、完成する。
			*
			*/
			//↓ログインidが空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if(!searchloginId.equals("")) {
				sql += " AND login_id Like '"+"%"+searchloginId+"%'";
			}

			//↓ユーザー名が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if(!searchname.equals("")) {
				sql += " AND name Like '"+"%"+searchname+"%'";
			}

			//↓生年月日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if(!searchBirthDate.equals("")) {
				sql += " AND birth_date  >=  '" + searchBirthDate + "'";
			}

			//↓生年月日（のここまでの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if(!searchBirthDate2.equals("")) {
				sql += " AND birth_date <= '" + searchBirthDate2 + "'";
			}


			// SELECTを実行し、結果表を取得
			ResultSet rs = stmt.executeQuery(sql);


			//↓検索機のを付ける前に書いていた検索用のsql文　（preparestatementで書いていたもの）
			// SELECT文を準備
			//	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
			//	            String sql = "SELECT * FROM user WHERE name LIKE ?";
			//	            											            // SELECTを実行し、結果表を取得
			//	        stmt = con.prepareStatement(sql);
			//	            stmt.setString(1,"%"+searchname+"%");
			//	            ResultSet rs = stmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(loginId, name, birthDate, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

}
