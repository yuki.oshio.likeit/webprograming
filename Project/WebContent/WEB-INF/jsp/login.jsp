<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <div  class="centering_item row">
    <h1  class=" col-sm-12 ">ログイン画面</h1>
    	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <form action="LoginServlet" method="post">
            <div class=" col" align="center">ログインID<input class="col " type="text" name="loginId" placeholder="ログインID">
            </div>
            <div class=" col" align="center">パスワード<input class="col" type="text" name="password" placeholder="パスワード" >
            </div>
            <p></p>
            <input type="submit" class="btn btn-primary" value="ログイン">
            </form>
    </div>
</body>
</html>