package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeansあとdaoクラスのメソッドでこのクラスにデータを入れたいとき、このクラスにコンストラクタを作って引数にしてデータを入れ込むためのコンストラクタ置き場でもある。
 * daoクラスのそれぞれのメソッドに対応するコンストラクタをオーバーロードでそれぞれ変数の数を対応させていくつも↓で作っている。
 *
 */
public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	// searchIdメソッド（入力されたloginIdの文字列で既に元のデータベースに存在するかを検索するメソッド）で入力された引数として使うためのコンストラクタ。JSP→servletで検索→データベースと参照するため、servletファイルのgetParameter()とJSPでの入力欄のname属性名と同一名にしている。
	public  User(String name) {
		this.loginId = name;

	}
	// ログイン画面のデータを保存するためのコンストラクタsearchIdメソッドで使うためのコンストラクタ
	public  User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// 他画面からの全ての入力データをここにセットするためのコンストラクタ(findAll用)
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//新規登録画面で入力されたデータを入れ込むメソッドを使うためのコンストラクタ(useraddメソッド用)引数をJSP、servlet、dao、元のデータベースのカラム名全て順番を揃えている。ここのUser.javaクラスを絡めたメソッドの引数名については、dao、sevletファイル全てのクラスで変数名を揃えている。
	public User( String loginId, String name,Date birthDate,  String password ) {
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
	}
	//userdetailでのfindrowメソッド用
	public User( String loginId, String name, Date birthDate,String createDate,String updateDate) {

		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//UPDATE用
	public User(String password, String name, Date birthDate) {
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;

	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}





}
