<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!--c:forEach等のリファレンス処理をかけ雨量にするために書くこと-->
<!DOCTYPE html>						<!-- ↑この2つの文のうち1行目のは、文字化けしないように初めにUTF-8と文字コードを設定する分で2行目は、c:forach等のEL式を書けるようにするために必要な文である。 -->
<html>
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <header>
    <ul class="nav justify-content-end navbarp">
        <li class="nav-item">
            <a class="nav-link active" href="#">ユーザー名</a>
        </li>
        <li class="nav-item">
           <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>

    </ul>
    </header>

    <div class="container">

        <h1 class="mtb30px">ユーザー一覧</h1>

<form  action="UserCreateServlet" method="get" class="form-horizontal">
            <div class="col newreg">
                <button type="submit" value="新規登録" class="btn btn-outline-success my-2 my-sm-0">新規登録</button>
            </div>
            </form>

    <form action="UserListServlet" method="post"  class="form-horizontal">		<!-- ←formタグで囲まないとacion属性で設定した先に入力した内容を渡すことができない。 -->
        <div  class="form-group row" >
            <label for="logid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
                <input type="text" id="logid" name="loginId">
            </div>
        </div>
        <div  class="form-group row" >
            <label for="un" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-10">
              <input type="text" id="un" name="name">
            </div>
        </div>
        <div  class="form-group row">
            <label for="bn" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-3">
              <input type="date" id="bn" name="birthDate">
            </div>
            <div class="col-sm-2 text-center">
            ~
            </div>
            <div class="col-sm-3">
              <input type="date" name="birthDate2">
            </div>
        </div>
        <div class="text-right">
        <button type="submit" value="検索" class="btn btn-outline-success my-2 my-sm-0">検索</button>
        </div>
    </form>
    </div>

  <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザー名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="user" items="${userList}" >	<!--←varに変数名userを宣言しそこにitemから取り出したデータを格納する。itemにはuserlistサーブレットクラスで作成したuserList配列を設定し、そこに入っているデータが格納され繰り返し表示される。で繰り返し表示させたい変数を設定し、設定した変数を並べたテーブルを作ってそれをforeachで囲む。-->
    <tr>
      	<td>${user.loginId}</td>
        <td>${user.name}</td>		<!-- ←繰り返し表示される項目。これら3つの変数でなくてもuserList配列内にあるのはパスワードや　登録日も入っているので追加でその変数名を書いても表示される。上から -->
        <td>${user.birthDate}</td>
      	<td>	<!-- 重要　↓ 　重要　　formタグでまずデータを渡す先のサーブレット名とdogetかdopostかの設定をし、且つaタグのhref属性で改めてデータを渡す先のサーブレット名と？idでバインド及び変数を用意する。で ＝ の後にサーブレットでsetAttributeされた渡したい変数名を$｛｝に書いてセットする。これでidの中に$｛user.id｝が入ったことになる。サーブレットで取得する際は、変数名であるidと記述して呼び出す必要がある。
      						これで例えば↓の詳細ボタンをクリックすると、formで設定された渡し先の設定したgetかpossメソッドにhrefで設定したデータが渡り、使えるようになる
      										ただし、その中のメソッド内で必ずgetParameterで取得しないとサーブレット内に入らず、使えていないことになるので注意。
      										また、getかpostに渡ったら、その渡った先でしか使えない。だからもしgetに渡したら、postでも使いたい場合、dogetメソッド内にsetAttibuteでidをセットし、渡した先のサーブレットのgetで表示したJSP内にinputタグのhiddenタイプでvalue属性の中にその変数を$｛｝で書くこと。
      											これでJSP内にidを隠して入れて置けて、formタグのpostでdopostメソッドにデータを渡して使えるようになる。-->
			<form method="get" action="Userdetailservlet" class="form-horizontal ">
				<a class="btn btn-primary" href="Userdetailservlet?id=${user.id}">詳細</a>
			</form>

<!--test属性に条件を↓書くこと -->	<!-- ↓このloginUser.loginIdは、loginサーブレットのpostメソッドでsession.setAttributeでセットされており、 -->
			<c:if test="${loginUser.loginId == 'admin'||loginUser.loginId == user.loginId}">
						<!-- ↑c:ifでの変数同士での文字比較は、この通りにできる。インスタンスをsetAttributeでセットした場合はキー名.変数名でインスタンスに入った変数をそれぞれ使うことができる。sessionでsetattributeしらならセッション破棄しない限り他画面でも呼び出すことができる。キー名.中に入っている変数名で呼び出すことに注意。 -->
			<form method="get" action="UserUpdateServlet" class="form-horizontal">
				<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
			</form>
			</c:if>

						<!-- ↓変数内にある文字と普通の文字との比較はこのとおりでできるシングルクォーテーションで囲むことに注意 -->
			<c:if test="${loginUser.loginId == 'admin'}">
			<form method="get" action="UserDeleteServlet" class="form-horizontal ">
				<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
			</form>
			</c:if>
      	</td>
    </tr>
    </c:forEach>
  </tbody>
</table>
</body>
</html>