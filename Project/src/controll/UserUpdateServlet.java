package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//userlistjspのhrefでここを指定し、自動採番idを取得且つformタグのmethod="get"でここに飛ぶ
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");

		// TODO Auto-generated method stub
		//userlist画面の詳細ボタンが押されたらこのdogetメソッドで処理をする
		String id=request.getParameter("id");

		//findall()で検索し入れ込んであるデータをこのサーブレットのセッションにセット
		UserDao userDao = new UserDao();
		User user = userDao.findrow(id);

		// リクエストスコープにユーザ一覧情報をセットここでまとまったインスタンスuserをセットしておけば、JSPファイルでuser.loginIdやuser.nameなどで書けるようになるサーブレットファイルで一々変数を用意しなくて済む。
		request.setAttribute("user", user);

		//このサーブレットのdopostメソッドにidを渡して使えるようにするために、JSPに書いたhiddenタグに用意した変数にデータをセットする
		request.setAttribute("id", id);

		//userdetailを表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

				// TODO Auto-generated method stub
				//userupdate.jsp画面の更新ボタンが押されたらこのdopostメソッドで処理をする
				//入力された値を取得  あとsql文で検索条件や、更新条件に使う要となる前のJSP画面のからもらってきた自動採番されたidも取得
				String password=request.getParameter("password");
				String passwordkakunin=request.getParameter("passwordkakunin");
				String name=request.getParameter("name");
				String birthdate=request.getParameter("birthdate");
				String id=request.getParameter("id");


				// passwordとpasswordkakuniが一致かつ、他の入力欄で空文字が入っていなければ（いずれも未入力がない場合）update実行し、UserListServeltへ遷移
				if(password.equals(passwordkakunin)&&!name.equals("")&&!birthdate.equals("")){
				//userdaoにあるupdatepメソッドを呼び出して使うために、UserDao型インスタンスを作成。
				UserDao userDao = new UserDao();
				//UserDao型インスタンス名.UserDao内にあるメソッド名で呼び出している。引数は、パスワード以外のユーザ名、生年月日と更新するために行を指定するための条件を付けるためのid
				userDao.updatep(password,name,birthdate,id);
				//userlist.jspにリダイレクト
				response.sendRedirect("UserListServlet");

				// passwordとpasswordkakuniが空欄で、他未入力がない場合update実行し、UserListServeltへ遷移つまりパスワード以外を更新させる
				}else if(password.equals("")&&passwordkakunin.equals("")&&!name.equals("")&&!birthdate.equals("")){
				//userdaoにあるupdateメソッドを呼び出して使うために、UserDao型インスタンスを作成。
				UserDao userDao = new UserDao();
				//UserDao型インスタンス名.UserDao内にあるメソッド名で呼び出している。引数は、パスワード以外のユーザ名、生年月日と更新するために行を指定するための条件を付けるためのid
				userDao.update(name,birthdate,id);
				//userlist.jspにリダイレクト
				response.sendRedirect("UserListServlet");

				//上記のifのいずれの条件にもあてはまらなかったら
				}else{
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg","入力された内容は正しくありません。");
				// userupdate.jspにフォワードして戻る
				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
				dispatcher.forward(request, response);
				return;

				}
		}

}
