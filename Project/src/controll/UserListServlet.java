package controll;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserListServlet() {
        super();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User user=(User)session.getAttribute("loginUser");

		if(user==null) {
			response.sendRedirect("LoginServlet");
			return;	 //←フォワードが、get　postメソッドの中で、2つ以上ある場合は、必ず、returnを書かないとgetメソッドの一番下、postメソッドの一番下まで読み込まれてからフォワード先を実行するのでreturnを書かないとどちらのフォワード先に行けばいいかpcがわからないので必ず、書くこと、正し、get　postメソッド中で一番下の方のforwardには、ほかにフォワードもないので下の方へ読み込みようがないので書かなくていい。
		}			//例えば、このリダイレクトのフォワードは、もしsessionにデータが残ってなければLoginServletにリダイレクトするという簡単な文なのでこれにreturnを付ければ、条件に引っかからなければ、あとは下に読み込むだけなのでreturnを書かなくても分岐したまま実行がなされる。
					//フォワードがget　postメソッドの中に複数ある場合、必ずreturnを書かないと、フォワードできませんとエラーが出る。


		// ユーザ一覧情報を取得
		//他クラスからリダイレクトでこのクラスに来ても既に入っているデータもuseraddで入力されformのactionでサーブレットへ→getParameter()を始めとしたUserCreateServletの処理で新しく追加されたデータもrequest.setAttribute()で追加済みのuserListつまり最新の状態のuserlistが表示される。
		//なぜなら↓にあるList<User> userList = userDao.findAll();でuserlistの中にfindallメソッドで全データを検索し、beansに入れ込まれるから。
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();



		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);



		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO  未実装：検索処理全般
		request.setCharacterEncoding("UTF-8");

		UserDao userDao = new UserDao();

		// セッションにユーザの情報をセット←と↓　これ間違いであるが、セッションでセットしなくてよいということ。ログイン画面で入力した情報を基に検索し、ほかのデータが読み込まれる構造で製作しているので、sessionでの情報が必要なのは認証データなので、ログインで入力したデータだけで十分だからloginUserだけsessionでsetAttributeでセットするので、ほかのデータはrequestで一画面でgetとかで取得して、扱っている。
		//HttpSession session = request.getSession();←　　↓のuserListのデータは認証に使うデータではないしsql等で保存されているデータなのでreqestで呼び出せば十分なのでsesisonの扱いは不要。　

		//セットする際に、内容が入ったインスタンスuserとそのインスタンスを入れ込んだ後、呼び出したり等したいときに使うkey名を設定。
		//request.setAttribute("userList", userList);  //セットする際に、内容が入ったインスタンスuserとそのインスタンスを入れ込んだ後、呼び出したり等したいときに使うkey名を設定。

		// ユーザー名検索の文字
		String searchloginId=request.getParameter("loginId");
		String searchname=request.getParameter("name");
		String searchBirthDate=request.getParameter("birthDate");
		String searchBirthDate2=request.getParameter("birthDate2");
		List<User> userList =userDao.namesearch(searchloginId,searchname,searchBirthDate,searchBirthDate2);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);

	}

}
