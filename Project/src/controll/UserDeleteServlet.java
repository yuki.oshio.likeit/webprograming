package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;


/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//userlist画面の詳細ボタンが押されたらこのdogetメソッドで処理をする
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//前の画面のデータidを取得及びsearchlogIdで持ってきたidを基にloginIdを取得
		String id=request.getParameter("id");
		UserDao dao=new UserDao();
		String loginId=dao.searchlogId(id);

		//持ってきたidをjspのhiddenにセットし、formのaction先のサーブレットのpostメソッドに渡させる。渡さないとpostメソッド内で扱う予定のidが何もない状態になり、空文字を扱っていることになり、何も削除していないこととなる。
		request.setAttribute("id", id);

		//確認用のlogin_id(削除される予定の方の)をセット
		request.setAttribute("loginId",loginId);
		//userdelete.jsp画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//前の画面のデータ取得
		String id=request.getParameter("id");


		//sql処理(ここでは削除処理)
		UserDao dao=new UserDao();
		dao.userdelete(id);


		//リダイレクト
		response.sendRedirect("UserListServlet");
	}

}
