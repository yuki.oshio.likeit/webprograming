<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!--c:forEach等のリファレンス処理をかけ雨量にするために書くこと-->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <header>
    <ul class="nav justify-content-end navbarp">
        <li class="nav-item">
            <a class="nav-link active" href="#">ユーザー名</a>
        </li>
        <li class="nav-item">
           <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>

    </ul>
    </header>

    <div class="container">

        <h1 class="mtb30px">ユーザー削除確認</h1>
        <p class="mtb30px">${loginId}</p>
        <p class="mtb30px">を本当に削除してよろしいでしょうか。</p>

        <div class="row">
        <form action="UserListServlet" method="get">
            <div class="col-sm-4 buttons">
                <button type="submit" value="検索" class="btn btn-outline-success my-2 my-sm-0">キャンセル</button>
            </div>
        </form>
            <div class="col-sm-4 buttons">
            	<form action="UserDeleteServlet" method="post">
            	<!-- ↓前の画面からhrefの？で変数をもってきたのはgetメソッドである。その変数を使える範囲はgetメソッドまでである。
            		postでも使えるようにするには、前のファイルのformタグのmethod="get"でデータもらって発動したdogetメソッドで表示された現在のJSP画面ファイルの中で必ずinputタグのhiddenタイプでvalue属性の中にその変数を$｛｝で書くこと。
            		で、dogetメソッド内でsetAttributeでidをセットしJSP内に前のデータを入れて置き、formタグのpostでdopostメソッドにデータを渡して使えるようになる。-->
            	<input type="hidden" value="${id}" name="id"></input>
                	<button type="submit" value="検索" class="btn btn-outline-success my-2 my-sm-0">OK</button>
                </form>
            </div>


        </div>

</div>


</body>
</html>