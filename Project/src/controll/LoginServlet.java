package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//1.文字コード設定
		request.setCharacterEncoding("UTF-8");

		//2.getParameter()で入力項目取得 ここの変数名はloginsessions()メソッドの引数と同じにする。入力された文字列を用意した変数に入れ込み、その変数を引数にloginnsessionsメソッドで判定するから。
		String loginId=request.getParameter("loginId");
		String password=request.getParameter("password");
		//3.取得した入力項目を入れた引数を使い、DAOに入れる
		UserDao dao=new UserDao();	//入力内容を入れ込み,その文字列で検索し、認証したいからdb接続及びsql文を実行した内容が入っているloginsessionsメソッドを使いたいからUserDao型のdaoというインスタンスを作る。
		User loginuser=dao.loginsessions(loginId,password);	//データを入れ込む(保存する)場所であるUserクラスの中のデータをloginsessions(入力した値の変数名,入力した値の変数名)メソッドで検索したいからUserクラスのインスタンスを作って。dao,loginsessions()でメソッドを呼び出し、検索している。

		//4.フォワード
		//4.1 ↑で検索した文字列のデータがUserBeansになかったら（つまりログインに失敗したら）
		if(loginuser==null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg","ログインに失敗しました。");

			// ログインjspにフォワードして戻る
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//4.2 テーブルに該当のデータが見つかった場合(ログイン成功時)
		// セッションにユーザの情報をセット
		/*セットする際に、内容が入ったインスタンスuserとそのインスタンスを入れ込んだ後、
		 * 呼び出したり等したいときに使うkey名を設定。これはsession.setAttributeなのでsessionだから、
		 * 値がスコープにセットされ続けているので、他画面でもキー名で呼び出すことができる。
		 * で、今回は、LoginServletのpostメソッドでsession.setAttributeでUser型のインスタンス
		 * であるloginUserに login画面で入力されたloginIdとpasswordがloginnsessionsメソッドによ
		 * り検索、格納されているloginUserがスコープの呼び出しで使われるキー名とともにセットされて
		 * いる。
		 *
		 * setAttributeでキー名が実際に入る変数名と一緒になっているが、これはわかりやすくするためだけであって、
		 * キー名を一緒にしなくてもいい。
		 * で、インスタンスを今回はセットしているわけだから、インスタンス名.中に入っている変数名
		 * 	の形と同じで、キー名.中に入っている変数名で、普通のインスタンスの様にJSP画面で${}
		 * の中で呼び出すことができる。sessionAttributeでセットされているのでセッションが破棄されない限り
		 * 他画面、他ファイルで扱うことができる。
		 * これを利用してc;ifの条件分岐等できるので活用すること。↓
		 * */
		HttpSession session = request.getSession();
		session.setAttribute("loginUser", loginuser);
		// ユーザ一覧のサーブレットにリダイレクト(getアクセス)
		response.sendRedirect("UserListServlet");
	}

}
