<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <header>
    <ul class="nav justify-content-end navbarp">
        <li class="nav-item">
            <a class="nav-link active" href="#">ユーザー名</a>
        </li>
        <li class="nav-item">
           <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>

    </ul>
    </header>

    <div  class="container">
    <h1>ユーザー新規登録</h1>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if>
    <form action="UserCreateServlet" method="post">

            <div class=" col passtxt" align="center">ログインID<input class="col " type="text" name="loginId"></div>

            <div class=" col passtxt" align="center">ユーザ名<input class="col" type="text" name="name"></div>

            <div class=" col passtxt" align="center">パスワード<input class="col" type="password" name="password"></div>

            <div class=" col passtxt" align="center">パスワード(確認)<input class="col" type="password" name="passwordkakunin"></div>

            <div class=" col passtxt" align="center">生年月日<input class="col" type="date" name="birth_date"></div>

            <div  class="col passtxt" align="center"><input type="submit" value="登録" ></div>
    </form>
    </div>

    <form action="UserListServlet" method="get">
    	<div  class="btn btn-primary btn-lg my-1 mb-5 px-5" align="center"><input type="submit" value="戻る" ></div>
    </form>
</body>
</html>