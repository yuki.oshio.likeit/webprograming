package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// ユーザ一覧情報を取得
		//セッション(useraddで入力されたデータがUserCreateServletの処理で新しく追加されそれも表示するためにセッションを作り、session.setAttribute()で追加済みのuserList)
		// リクエストスコープにユーザ一覧情報をセット
		request.getAttribute("userList");

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO  未実装：検索処理全般

		//1.文字コード設定
		request.setCharacterEncoding("UTF-8");

			//2.getParameter()で入力項目取得 ここの変数名はloginsessions()メソッドの引数と同じにする。入力された文字列を用意した変数に入れ込み、その変数を引数にloginnsessionsメソッドで判定するから。
			String loginId=request.getParameter("loginId");
			String name=request.getParameter("name");
			String password=request.getParameter("password");
			String passwordkakunin=request.getParameter("passwordkakunin");
			String birthDate=request.getParameter("birth_date");


			//同じログインidないか探す
			UserDao userDao = new UserDao();
			String e = userDao.searchId(loginId);
			//passwordとpasswordkakuninが一致しなかった場合または↑で取得されるべき項目が1つでも空だったら　Stringがからの場合nullではなく「""」が入る（空文字）のでequals("")としている
			//if文条件で、e！=nullは、searchIdで「登録予定のloginIdに入っている文字列」がbeansやデータベースにすでに入っていないかメソッドで確認するのだが、これは
			if(!(password.equals(passwordkakunin))||e != null||loginId.equals("") || name.equals("") || password.equals("") || passwordkakunin.equals("") || birthDate.equals("") ) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg","入力された内容は正しくありません。");
				// useradd.jspにフォワードして戻る
				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
				dispatcher.forward(request, response);
				return;

			}else{
				//3.取得した入力項目を入れた引数を使い、DAOに入れる
				UserDao dao=new UserDao();	//入力内容を入れ込み,その文字列で検索し、認証したいからdb接続及びsql文を実行した内容が入っているloginsessionsメソッドを使いたいからUserDao型のdaoというインスタンスを作る。
				dao.useradd(loginId,name,birthDate,password);	//データを入れ込む(保存する)場所であるUserクラスの中のデータをloginsessions(入力した値の変数名,入力した値の変数名)メソッドで検索したいからUserクラスのインスタンスを作って。dao,loginsessions()でメソッドを呼び出し、検索している。

				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");
			}
	}

}
