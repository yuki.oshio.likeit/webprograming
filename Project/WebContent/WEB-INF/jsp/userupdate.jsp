<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!--c:forEach等のリファレンス処理をかけ雨量にするために書くこと-->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <header>
    <ul class="nav justify-content-end navbarp">
        <li class="nav-item">
            <a class="nav-link active" href="#">ユーザー名</a>
        </li>
        <li class="nav-item">
           <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>

    </ul>
    </header>

    <div class="container">
        <h1 class="userdetail">ユーザー情報詳細更新</h1>
    <div  class="row">	<!--↓ c:ifは判定して条件に当てはまったら、c:ifで囲んだ文字列や変数を出力するためのタグである。なので値を渡したり、ほかの処理をこのタグではさせることはできない。-->
<c:if test="${errMsg != null}" >  <!-- ←エラーメッセージの場所を用意　もしerrMsg内がから出なかったらそれを表示させる処理。 -->
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}	<!--←この中にサーブレットでsetAttributeでセットされた内容が表示される。勘違いしてはいけないのがこのerrMsgはあくまで1変数と何ら変わらない。サーブレットのsetattributeで、キー名と実際に入るデータでセットするが、ここのデータは、文字列もセットすることができる。	で、このerrMsgにはその文字列がsetattributeで入っている。なので↑のifで値が入っていると判定され、errMsgの中身が表示されるという仕組みなのである。  -->

		</div>
	</c:if>
    </div>

    <form  action="UserUpdateServlet" method="post" class="form-horizontal">
			<!-- ↓前の画面からhrefの？で変数をもってきたのはgetメソッドである。その変数を使える範囲はgetメソッドまでである。
            		postでも使えるようにするには、前のファイルのformタグのmethod="get"でデータもらって発動したdogetメソッドで表示された現在のJSP画面ファイルの中で必ずinputタグのhiddenタイプでvalue属性の中にその変数を$｛｝で書くこと。
            		で、dogetメソッド内でsetAttributeでidをセットしJSP内に前のデータを入れて置き、formタグのpostでdopostメソッドにデータを渡して使えるようになる。-->
    	<input type="hidden" value="${id}" name="id">

        <div  class="form-group row">
        	<label for="loginid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-2" id="loginid">
                <p>${user.loginId}</p>
            </div>	<!-- ↑サーブレットファイルのdogetメソッド内でsetAttriute("キー名",実際にセットするサーブレットの変数)でここに変数やインスタンスをセットできる。インスタンスをセットすると、そのインスタンス内の変数もインスタンス名.変数名で使用でき、サーブレットのsetAttributeで一々変数をセットしなくて済むことがあるので、インスタンスにデータを入れたときは、可能な限りインスタンスでsetAttributeでセットすること。-->
        </div>

        <div  class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">パスワード</label>
            <div class="col-sm-10" id="password">
              <input class="col " type="text" name="password">
            </div>
        </div>

        <div  class="form-group row">
            <label for="kakunin" class="col-sm-2 col-form-label">パスワード(確認)</label>
            <div class="col-sm-10" id="kakunin">
              <input class="col " type="text" name="passwordkakunin">
            </div>
        </div>

        <div  class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">ユーザー名</label>
            <div class="col-lg-10" id="name">
              <input class="col" value="${user.name}" type="text" name="name">
            </div>						<!-- ↑inputタグ内のvalue属性で、$｛｝で変数を入れるとその変数に入った文字列やデータが、記入欄の初期表示として表示される。なので、生年月日や　名前等、登録されている情報を更新する際に使用されるので覚えて置くこと、 -->
        </div>

        <div  class="form-group row">
            <label for="birth" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-10" id="birth">
              <input class="col" value="${user.birthDate}" type="text" name="birthdate">
            </div>
        </div>
        <div class="submitbutton" >
            <button type="submit" value="更新" class="btn btn-outline-success my-2 my-sm-0">更新</button>
        </div>
    </form>

    </div>
    <div>
    	<form action="UserListServlet" method="get">
            <div class="col-sm-4 buttons">
                <button type="submit" value="戻る" class="btn btn-outline-success my-2 my-sm-0">戻る</button>
            </div>
        </form>
    </div>
</body>
</html>