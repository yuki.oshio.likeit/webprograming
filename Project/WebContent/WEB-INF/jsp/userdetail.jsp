<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!--c:forEach等のリファレンス処理をかけ雨量にするために書くこと-->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="s.css" rel="stylesheet"type="text/css"/>
</head>
<body>
    <header>
    <ul class="nav justify-content-end navbarp">
        <li class="nav-item">
            <a class="nav-link active" href="#">ユーザー名</a>
        </li>
        <li class="nav-item">
           <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>

    </ul>
    </header>

    <div class="container">
        <h1 class="userdetail">ユーザー情報詳細参照</h1>

        <div  class="form-group row">
            <label for="logid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">

                <p class="form-control-plaintext">${user.loginId}</p>
            </div>
        </div>

        <div  class="form-group row">
            <label for="un" class="col-sm-2 col-form-label">ユーザー名</label>
            <div class="col-lg-10">
              <p class="form-control-plaintext">${user.name}</p>
            </div>

        </div>
        <div  class="form-group row">
            <label for="un" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-10">
              <p class="form-control-plaintext">${user.birthDate}</p>
            </div>
        </div>

        <div  class="form-group row">
            <label for="un" class="col-sm-2 col-form-label">登録日時</label>
            <div class="col-sm-10">
              <p class="form-control-plaintext">${user.createDate}</p>
            </div>
        </div>

        <div  class="form-group row">
            <label for="bn" class="col-sm-2 col-form-label">更新日時</label>
            <div class="col-sm-3">
              <p class="form-control-plaintext">${user.updateDate}</p>
            </div>
        </div>

        <form action="UserListServlet" method="get">
        	<div class="text-left">
        	<button type="submit" value="検索" class="btn btn-outline-success my-2 my-sm-0">戻る</button>
        	</div>
		</form>
    </div>
</body>
</html>